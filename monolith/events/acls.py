import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_picture(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": city + " " + state
    }
    headers = {
        "Authorization": PEXELS_API_KEY
        }
    response = requests.get(url, params=params, headers=headers)
    picture = json.loads(response.content)

    city_pic = {
        "picture_url": picture["photos"][0]["src"]["original"]
    }
    return city_pic


def get_weather_data(city, state):
    params = {
        "q": city + ", " + state + ", USA",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    headers = {
        "Authorization": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params, headers=headers)
    weather_data = json.loads(response.content)

    latitude = weather_data[0]["lat"]
    longitude = weather_data[0]["lon"]

    params_2 = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    url_2 = "https://api.openweathermap.org/data/2.5/weather"
    response_2 = requests.get(url=url_2, params=params_2, headers=headers)
    data = json.loads(response_2.content)

    weather = {
        "temp": data["main"]["temp"],
        "description": data["weather"][0]["description"]
    }
    return weather
