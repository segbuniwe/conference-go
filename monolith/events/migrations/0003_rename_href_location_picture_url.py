# Generated by Django 4.0.3 on 2023-05-11 17:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_href'),
    ]

    operations = [
        migrations.RenameField(
            model_name='location',
            old_name='href',
            new_name='picture_url',
        ),
    ]
