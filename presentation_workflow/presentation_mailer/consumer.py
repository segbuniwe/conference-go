import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        def process_approval(ch, method, properties, body):
            print("  Received approval: %r" % body)
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            presenter_email = data["presenter_email"]
            title = data["title"]

            send_mail(
                    "Your presentation has been accepted",
                    f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
                    "admin@conference.go",
                    [presenter_email],
                )

        def process_rejection(ch, method, properties, body):
            print("  Received rejection: %r" % body)
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            presenter_email = data["presenter_email"]
            title = data["title"]

            send_mail(
                    "Your presentation has been rejected",
                    f"{presenter_name}, we're sad to tell you that your presentation {title} has been rejected",
                    "admin@conference.go",
                    [presenter_email],
                )

        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel2 = connection.channel()
        channel2.queue_declare(queue="presentation_rejections")
        channel2.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
        channel2.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
